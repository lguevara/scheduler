import subprocess
from time import sleep
from datetime import datetime, timedelta
import shutil
import pathlib
from random import random

from logger import log

"""
:param save_path:
:param remove_days 
"""


def pull():
    output = subprocess.Popen(
        ['git','reset','--hard', 'origin/master' ],
        stdout=subprocess.PIPE).communicate()[0]

    output += subprocess.Popen(
        ['git','pull'],
        stdout=subprocess.PIPE).communicate()[0]
    log.info(output)




