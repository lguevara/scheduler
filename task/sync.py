import subprocess
from time import sleep
from datetime import datetime, timedelta
import shutil
import pathlib
from random import random

from logger import log

"""
:param save_path:
:param remove_days 
"""


def sync(user,remote_ip,remote_path,local_path="/data/video"):
    print(user,remote_ip,remote_path,local_path)
    output = subprocess.Popen(
        ['ping', '-n', '1', '-w', '3', str(remote_ip)],
        stdout=subprocess.PIPE).communicate()[0]

    if "Destination host unreachable" in output.decode('utf-8'):
        log.info(str(remote_ip)+ " is Offline")
    elif "Request timed out" in output.decode('utf-8'):
        log.info(str(remote_ip)+ " is Offline")
    else:
        log.info(str(remote_ip) + " is Online")
        output = subprocess.Popen(
            #['rsync','--bwlimit=8730', '--partial', '--remove-source-files', '-r', '-t', '/tmp/test.img', user+"@"+str(ip)+":"+path],
            ['rsync','--partial', '--remove-source-files', '-r', '-t', local_path, user+"@"+str(remote_ip)+":"+remote_path],
            stdout=subprocess.PIPE).communicate()[0]



def bomb():
    f = open("/tmp/0", "w+")
    serial = random()*100
    t1 = datetime.now()
    while (datetime.now() - t1).seconds <= 10000:
        f.writelines(str(serial) + str(datetime.now()) + "\n")
        t2 = datetime.now()
        while (datetime.now() - t2).seconds <= 2:
            pass

    f.close()
