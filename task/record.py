import time
import json
import os
from datetime import datetime, date, timedelta
from time import sleep
import shutil
import pathlib
from pathlib2 import Path, PurePath
from subprocess import Popen, PIPE

from logger import log


def record(source,config):
    day_timestamp = '{:%Y-%m-%d}'.format(datetime.now())
    print(source)
    source = {key: str(val) for key, val in source.items()}
    print(source)
    save_dir = os.path.join(config["record"]["save_path"],config["record"]["movil"], day_timestamp, source["id"])
    path = Path(save_dir)

    if not os.path.isdir(save_dir):
        path.mkdir(parents=True, exist_ok=True)
        # os.mkdir(save_dir, exist_ok=True,parents=True )
    # timestamp = '{:%Y-%m-%day_timestampd %H:%M:%S}'.format(datetime.now())


    print("RUN")

    cmdline = [
        'ffmpeg',
        "-rtsp_transport",
        "tcp",
        '-i',
        source["input"],
        "-reset_timestamps",
        "1",
        "-strftime",
        "1",
        "-c:v",
        "copy",
        #"libx264",
        #'-c:a',
        #'copy',
        '-map',
        '0',
        '-f',
        'segment',
        '-segment_time',
        source["segments"],
        '-segment_format',
        source["format"],
        "-max_muxing_queue_size", "9999",
        save_dir + "/" + source["id"] + '-%Y-%m-%d_%H:%M:%S.' + source["format"],
        '-loglevel',
        'warning'
    ]
    print(" ".join(cmdline))

    timeout = time.time() + int(source["duration"])*60  # 5 minutes from now

    while True:
        if time.time() > timeout:
            break

        p = Popen(cmdline, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        pout, perr = p.communicate()
        pout = pout.decode("utf-8")
        perr = perr.decode("utf-8")

        if pout is not None and pout is not '':
            log.info(pout)

        if perr is not None and perr is not '':
            log.error(perr)

            if perr.find('Connection timed out') != -1:
                pass
            if perr.find('No route to host') != -1:
                pass
            if perr.find("No space left on device"):
                pass
            time.sleep(10)

        log.info("Return Code: " + str(p.returncode))
        # if p.returncode != 0:
        #    print("Exit 0")

        # log_file.close()
