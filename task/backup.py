from datetime import datetime, timedelta
import shutil
import pathlib
from logger import log

"""
:param save_path:
:param remove_days 
"""


def removeDays(days,path):
    log.info(path)
    list(map(
        shutil.rmtree,
        [str(x) for x in pathlib.Path(str(path)).glob('*')
         if datetime.strptime(x.parts[-1], "%Y-%m-%d") <
         datetime.now() - timedelta(days)]
    ))
#    print([x for x in pathlib.Path("/data/records").glob('*')
#         if datetime.strptime(x.parts[-1], "%Y-%m-%d") <
#         datetime.now() - timedelta(2)])


def backup(conf):
    list(map(
        shutil.rmtree,
        [x for x in pathlib.Path(conf["save_path"]).glob('*')
         if datetime.strptime(x.parts[-1], "%Y-%m-%d") <
         datetime.now() - timedelta(2)]
    ))