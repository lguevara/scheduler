# coding=utf-8
# !/bin/python
# TODO:
# Change blocking to non - blocking scheduler *
# change service type in systemd simple to forking *
# move current record to directory
# periodically classify records by record date

import time
import json
import os
from datetime import datetime, date, timedelta
from time import sleep


sleep_time = 15

from pathlib import Path, PurePath

import shutil

import pathlib
from apscheduler.events import EVENT_SCHEDULER_STARTED, EVENT_SCHEDULER_SHUTDOWN, EVENT_SCHEDULER_PAUSED, \
    EVENT_SCHEDULER_RESUMED, EVENT_EXECUTOR_ADDED, EVENT_EXECUTOR_REMOVED, EVENT_JOBSTORE_ADDED, EVENT_JOBSTORE_REMOVED, \
    EVENT_ALL_JOBS_REMOVED, EVENT_JOB_ADDED, EVENT_JOB_REMOVED, EVENT_JOB_MODIFIED, EVENT_JOB_SUBMITTED, \
    EVENT_JOB_MAX_INSTANCES, EVENT_JOB_EXECUTED, EVENT_JOB_ERROR, EVENT_JOB_MISSED, EVENT_ALL

from pytz import utc

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor

from subprocess import Popen, PIPE

config_path = "config.json"

with open(config_path) as f:
    read_data = f.read()

config = json.loads(read_data)

job_defaults = {
    'coalesce': True,
    # 'max_instances': 6,
    'misfire_grace_time': 1
}


from logger import log
import task.record as record

events = {

    EVENT_SCHEDULER_STARTED: "The scheduler was started 	SchedulerEvent",
    EVENT_SCHEDULER_SHUTDOWN: "The scheduler was shut down 	SchedulerEvent",
    EVENT_SCHEDULER_PAUSED: "Job processing in the scheduler was paused 	SchedulerEvent",
    EVENT_SCHEDULER_RESUMED: "Job processing in the scheduler was resumed 	SchedulerEvent",
    EVENT_EXECUTOR_ADDED: "An executor was added to the scheduler 	SchedulerEvent",
    EVENT_EXECUTOR_REMOVED: "An executor was removed to the scheduler 	SchedulerEvent",
    EVENT_JOBSTORE_ADDED: "A job store was added to the scheduler 	SchedulerEvent",
    EVENT_JOBSTORE_REMOVED: "A job store was removed from the scheduler SchedulerEvent",
    EVENT_ALL_JOBS_REMOVED: "All jobs were removed from either all job stores or one particular job store 	SchedulerEvent",
    EVENT_JOB_ADDED: "A job was added to a job store 	JobEvent",
    EVENT_JOB_REMOVED: "A job was removed from a job store 	JobEvent",
    EVENT_JOB_MODIFIED: "A job was modified from outside the scheduler 	JobEvent",
    EVENT_JOB_SUBMITTED: "A job was submitted to its executor to be run 	JobSubmissionEvent",
    EVENT_JOB_MAX_INSTANCES: "A job being submitted to its executor was not accepted by the executor because the job has already reached its maximum concurrently executing instances MissionEvent",
    EVENT_JOB_EXECUTED: "A job was executed successfully 	JobExecutionEvent",
    EVENT_JOB_ERROR: "A job raised an exception during execution 	JobExecutionEvent",
    EVENT_JOB_MISSED: "A job’s execution was missed 	JobExecutionEvent",
    EVENT_ALL: "EVENT ALL"
}


def events_all(event):
    # print(event.job_id)
    # if event or event.exception:
    if event:
        #log.info(event.__dict__)
        log.info(events[event.code])
    if event.code == EVENT_JOB_ERROR:
        log.error(events[event.code])
        # print("!!! EXCEPT !!!")


if __name__ == '__main__':

    scheduler = BlockingScheduler()
    #scheduler = BackgroundScheduler()

    url = "sqlite:///jobs.sqlite"
    scheduler.add_jobstore('sqlalchemy', url=url)
    # scheduler.add_jobstore('memory')
    scheduler.add_executor('processpool', max_workers=10)

    for job in scheduler.get_jobs():
        print(job.id)


    ### sync
    from task.sync import sync
    scheduler.add_job(sync, 'cron', id="sync",
                      args=[
                          config["sync"]["user"],
                          config["sync"]["remote_ip"],
                          config["sync"]["remote_path"],
                          config["sync"]["local_path"]],
                      #args=[],
                      # year='*',
                      # month='*',
                      # day='*',
                      # week='*',
                      day_of_week='*',
                      hour='*',
                      minute='*',
                      # second='*',
                      # misfire_grace_time=60*60*24*365,
                      replace_existing=True,
                      # start_date=date(2017, 12, 6),
                      # next_run_time=datetime.now() - timedelta(minutes=60 * 24),
                      next_run_time=datetime.now() + timedelta(seconds=2),
                      #next_run_time=datetime.now() + timedelta(seconds=5),
                      misfire_grace_time=60 * 60 * 24
                      # timezone=utc
                      )

    #Counter
    # from task.counter import counter
    # scheduler.add_job(counter, 'cron', id="counter",
    #                   args=[
    #                       ],
    #                   # args=[],
    #                   # year='*',
    #                   # month='*',
    #                   # day='*',
    #                   # week='*',
    #                   day_of_week='*',
    #                   hour='*',
    #                   minute='*',
    #                   # second='*',
    #                   # misfire_grace_time=60*60*24*365,
    #                   replace_existing=True,
    #                   # start_date=date(2017, 12, 6),
    #                   # next_run_time=datetime.now() - timedelta(minutes=60 * 24),
    #                   next_run_time=datetime.now() + timedelta(seconds=2),
    #                   # next_run_time=datetime.now() + timedelta(seconds=5),
    #                   misfire_grace_time=60 * 60 * 24
    #                   # timezone=utc
    #                   )

    ##daily jobs
    from task.backup import removeDays
    scheduler.add_job(removeDays, 'cron', id="daily",
                      args=[config["record"]["remove_days"],os.path.join(config["record"]["save_path"],config["record"]["movil"])],
                      # year='*',
                      # month='*',
                      # day='*',
                      # week='*',
                      day_of_week='*',
                      hour='*',
                      minute='*',
                      #second='*',
                      # misfire_grace_time=60*60*24*365,
                      replace_existing=True,
                      # start_date=date(2017, 12, 6),
                      # next_run_time=datetime.now() - timedelta(minutes=60 * 24),
                      next_run_time=datetime.now() + timedelta(seconds=5),
                      misfire_grace_time=60 * 60 * 24
                      # timezone=utc
                      )

    # config file
    for source in config["record"]["sources"]:
        print(source["id"])
        #record = record.record(conf)
        #conf["save_dir"] = "./records"
        scheduler.add_job(record.record, 'cron', id=source['id'],
                          args=[source,config],
                          # year='*',
                          # month='*',
                          # day='*',
                          # week='*',
                          day_of_week='*',
                          hour='1',
                          minute='7',
                          # second='*',
                          # misfire_grace_time=60*60*24*365,
                          replace_existing=True,
                          # start_date=date(2017, 12, 6),
                          # next_run_time=datetime.now() - timedelta(minutes=60 * 24),
                          next_run_time=datetime.now() + timedelta(seconds=5),
                          misfire_grace_time=60 * 60 * 24
                          # timezone=utc
                          )

    scheduler.print_jobs()

    scheduler.add_listener(events_all)
    print('Press CtrBlockingScheduler()l+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

    for job in scheduler.get_jobs():
        print(job.id)
        # job.remove()

    try:
        scheduler.start()
        scheduler.print_jobs()
        # scheduler.print_jobs()
        # scheduler.resume_job("cam1")
        # print(datetime.now())
        # print(datetime.now() - timedelta(minutes=60*24))
#        while 1:
#            inp = input(">")
#            exec(inp)

    except (KeyboardInterrupt, SystemExit):
        print("Exit")
