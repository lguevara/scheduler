import logging

logger = logging.getLogger()  # this returns the root logger
logger.addHandler(logging.StreamHandler())
log = logging.getLogger('apscheduler.executors.default')
log.setLevel(logging.INFO)
# fmt = logging.Formatter(':%(asctime)s : %(levelname)s : %(name)s : %(message)s',"%Y-%m-%d %H:%M:%S")
fmt = logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s', "%Y-%m-%d %H:%M:%S")
ch = logging.StreamHandler()
ch.setFormatter(fmt)
log.addHandler(ch)
fh = logging.FileHandler("./log_planificador.txt")
fh.setFormatter(fmt)
log.addHandler(fh)
