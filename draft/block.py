"""
Demonstrates how to use the blocking scheduler to schedule a job that executes on 3 second
intervals.
"""

from datetime import datetime
import os

from apscheduler.executors.pool import ProcessPoolExecutor
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.schedulers.blocking import BlockingScheduler
from pytz import utc


import logging

#logger = logging.getLogger()  # this returns the root logger
#logger.addHandler(logging.StreamHandler())

#log = logging.getLogger('apscheduler.executors.default')
#log.setLevel(logging.INFO)
#
#fmt = logging.Formatter('%(levelname)s:%(name)s:%(message)s')
#h = logging.StreamHandler()
#h.setFormatter(fmt)
#log.addHandler(h)


jobstores = {
    'default': SQLAlchemyJobStore(url='sqlite:///jobs.sqlite')
}
executors = {
    'default': ProcessPoolExecutor(5),
    #'processpool': ProcessPoolExecutor(5)
}
job_defaults = {
    'coalesce': False,
    'max_instances': 6
}


def tick1():
    print('Tick1! The time is: %s' % datetime.now())

def tick2():
    print('Tick2! The time is: %s' % datetime.now())


if __name__ == '__main__':
    scheduler = BlockingScheduler()
    url = "sqlite:///jobs.sqlite"
    scheduler.add_jobstore('sqlalchemy', url=url)
    #scheduler.add_executor('processpool',max_workers=1)
    #scheduler.configure(jobstores=jobstores, executors=executors, job_defaults=job_defaults, timezone=utc)
    scheduler.add_job(tick1, 'interval', seconds=5)
    scheduler.add_job(tick2, 'interval', seconds=3)
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass
